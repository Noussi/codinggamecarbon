package fr.efrei.carbon.codingameapi.models;

import fr.efrei.carbon.codingameapi.models.Campaign;
import org.springframework.data.annotation.PersistenceConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
public class Technology {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @ManyToMany(
            cascade = CascadeType.ALL,
            mappedBy = "technology"
    )
    private List<Campaign> campaigns;

    @PersistenceConstructor
    public Technology(int id, String name) {
        this.id = id;
        this.name = name;
    }

    protected Technology() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
