package fr.efrei.carbon.codingameapi.models;
import org.springframework.data.annotation.PersistenceConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
public class Campaign {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @ManyToOne(optional = false)
    @JoinColumn(name = "level_id", nullable = false)
    private Level level;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "campaign_technology",
            joinColumns = {@JoinColumn(name = "campaign_id")},
            inverseJoinColumns = {@JoinColumn(name="technology_id")}
    )
    private List<Technology> technology;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "evaluation",
            joinColumns = {@JoinColumn(name = "campaign_id")},
            inverseJoinColumns = {@JoinColumn(name="applicant_id")})
    private List<Applicant>  applicants;

    @ManyToMany(
            cascade = CascadeType.ALL,
            mappedBy = "campaigns"
    )
    private List<Evaluator> evaluators;

    public Campaign(String name, Level level, List<Technology> technology) {
        this.name = name;
        this.level = level;
        this.technology = technology;
    }

    @PersistenceConstructor
    public Campaign(String name, Level level) {
        this.name = name;
        this.level = level;
    }

    protected Campaign() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Level getLevel() {
        return level;
    }

    public List<Technology> getTechnology() {
        return technology;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public void setTechnology(List<Technology> technology) {
        this.technology = technology;
    }

    public void setApplicants(List<Applicant> applicants) {
        this.applicants = applicants;
    }

    public void setEvaluators(List<Evaluator> evaluators) {
        this.evaluators = evaluators;
    }
}
