package fr.efrei.carbon.codingameapi.models;

import org.springframework.data.annotation.PersistenceConstructor;

import javax.persistence.*;
import java.time.Duration;
import java.time.LocalDate;
import java.util.Objects;

@Entity
public class Evaluation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "campaign")
    private Campaign campaign;

    @ManyToOne
    @JoinColumn(name = "applicant")
    private Applicant applicant;

    @ManyToOne
    @JoinColumn(name = "evaluator")
    private Evaluator evaluator;

    @Column(name = "date_of_eval")
    private LocalDate dateOfEvaluation;

    @Column(name = "duration_of_eval")
    private Duration durationOfEvaluation;

    @Column(name = "duration_of_applicant")
    private Duration durationOfApplicant;

    @Column(name = "score")
    private int score;

    @PersistenceConstructor
    public Evaluation(int id, Campaign campaign, Applicant applicant, LocalDate dateOfEvaluation, Duration durationOfEvaluation, Duration durationOfApplicant) {
        this.id = id;
        this.campaign = campaign;
        this.applicant = applicant;
        this.dateOfEvaluation = dateOfEvaluation;
        this.durationOfEvaluation = durationOfEvaluation;
        this.durationOfApplicant = durationOfApplicant;
    }

    protected Evaluation() {
    }

    public int getId() {
        return id;
    }

    public Campaign getCampaign() {
        return campaign;
    }

    public Applicant getApplicant() {
        return applicant;
    }

    public Evaluator getEvaluator() {
        return evaluator;
    }

    public LocalDate getDateOfEvaluation() {
        return dateOfEvaluation;
    }

    public Duration getDurationOfEvaluation() {
        return durationOfEvaluation;
    }

    public Duration getDurationOfApplicant() {
        return durationOfApplicant;
    }

    public int getScore() {
        return score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Evaluation)) return false;
        Evaluation that = (Evaluation) o;
        return id == that.id && score == that.score && Objects.equals(campaign, that.campaign) && Objects.equals(applicant, that.applicant) && Objects.equals(dateOfEvaluation, that.dateOfEvaluation) && Objects.equals(durationOfEvaluation, that.durationOfEvaluation) && Objects.equals(durationOfApplicant, that.durationOfApplicant);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, campaign, applicant, dateOfEvaluation, durationOfEvaluation, durationOfApplicant, score);
    }
}
