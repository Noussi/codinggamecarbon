package fr.efrei.carbon.codingameapi.models;

import org.springframework.data.annotation.PersistenceConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "question")
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "response")
    private String response;

    @Column(name = "wording")
    private String wording;

    @Column(name = "level")
    private String level;

    @OneToOne(optional = false)
    @JoinColumn(name = "technology_id", nullable = false)
    private Technology technology;

    @OneToMany
    private List<ItemQuestion> questions;

    @PersistenceConstructor
    public Question(String response, String wording, String level, Technology technology, List<ItemQuestion> questions) {
        this.response = response;
        this.wording = wording;
        this.level = level;
        this.technology = technology;
        this.questions = questions;
    }

    protected Question() {
    }


    public int getId() {
        return id;
    }

    public String getResponse() {
        return response;
    }

    public String getWording() {
        return wording;
    }

    public String getLevel() {
        return level;
    }

    public Technology getTechnology() {
        return technology;
    }

    public List<ItemQuestion> getQuestions() {
        return questions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Question)) return false;
        Question question = (Question) o;
        return id == question.id && Objects.equals(response, question.response) && Objects.equals(wording, question.wording) && Objects.equals(level, question.level) && Objects.equals(technology, question.technology) && Objects.equals(questions, question.questions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, response, wording, level, technology, questions);
    }
}
