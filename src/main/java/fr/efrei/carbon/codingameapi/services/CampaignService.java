package fr.efrei.carbon.codingameapi.services;

import fr.efrei.carbon.codingameapi.repositories.CampaignsRepository;
import fr.efrei.carbon.codingameapi.exceptions.EntityNotFoundException;
import fr.efrei.carbon.codingameapi.repositories.LevelsRepository;
import fr.efrei.carbon.codingameapi.models.Campaign;
import fr.efrei.carbon.codingameapi.repositories.TechnologiesRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CampaignService {

    private final CampaignsRepository campaignsRepository;
    private final LevelsRepository levelsRepository;
    private final TechnologiesRepository technologiesRepository;

    public CampaignService(CampaignsRepository campaignsRepository, LevelsRepository levelsRepository, TechnologiesRepository technologiesRepository) {
        this.campaignsRepository = campaignsRepository;
        this.levelsRepository = levelsRepository;
        this.technologiesRepository = technologiesRepository;
    }

    public List<Campaign> getAllCampaigns() {
        return campaignsRepository.findAll();
    }

    public Optional<Campaign> findCampaignById(int campaignId) {
        return campaignsRepository.findById(campaignId);
    }

    public Campaign createCampaign(Campaign campaign) {
        final var level = levelsRepository.findById(campaign.getLevel().getId())
                                                 .orElseThrow(() -> new EntityNotFoundException("Level not found with id " +campaign.getLevel().getId()));
        final var campaign1 = new Campaign(campaign.getName(), level);
        return campaignsRepository.save(campaign1);
    }

    public void deleteCampaign(int id){
        final var campaignToDelete = campaignsRepository
                .findById(id)
                .orElseThrow(()-> generateNotFoundException(id));
        campaignsRepository.deleteById(id);
    }
    public Optional<Campaign> updateCampaign(int id, Campaign campaign){
        return campaignsRepository
                .findById(id)
                .map(campaignToUpdate -> {
                    campaignToUpdate.setName(campaign.getName());
                    campaignToUpdate.setLevel(campaign.getLevel());
                    return campaignToUpdate;
                })
                .map(campaignsRepository::save);
    }
    private EntityNotFoundException generateNotFoundException(int id) {
        return new EntityNotFoundException("campaign not found with id " + id);
    }
}
