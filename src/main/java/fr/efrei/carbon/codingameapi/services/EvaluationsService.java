package fr.efrei.carbon.codingameapi.services;

import fr.efrei.carbon.codingameapi.repositories.EvaluationsRepository;
import fr.efrei.carbon.codingameapi.models.Evaluation;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EvaluationsService {

    private final EvaluationsRepository evaluationsRepository;

    public EvaluationsService(EvaluationsRepository evaluationsRepository) {
        this.evaluationsRepository = evaluationsRepository;
    }

    public List<Evaluation> getEvaluations(){
        return evaluationsRepository.findAll();
    }
}
