package fr.efrei.carbon.codingameapi.services;
import fr.efrei.carbon.codingameapi.repositories.ApplicantsRepository;
import fr.efrei.carbon.codingameapi.exceptions.EntityNotFoundException;
import fr.efrei.carbon.codingameapi.models.Applicant;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class ApplicantsService {

    private final ApplicantsRepository applicantsRepository;

    public ApplicantsService(ApplicantsRepository applicantsRepository) {
        this.applicantsRepository = applicantsRepository;
    }

    public List<Applicant> getApplicants() {
        return applicantsRepository.findAll();
    }

    public Applicant createApplicant(Applicant applicant) {
        return applicantsRepository.save(applicant);
    }

    public Optional<Applicant> findById(int id) {
        return applicantsRepository.findById(id);
    }

    public Optional<Applicant> updateApplicant(int id,Applicant applicant) {
       return applicantsRepository
                .findById(id)
                .map(applicantToUpdate -> {
                    applicantToUpdate.setFirstName(applicant.getFirstName());
                    applicantToUpdate.setLastName(applicant.getLastName());
                    applicantToUpdate.setEmail(applicant.getEmail());
                    applicantToUpdate.setTraining(applicant.getTraining());
                    return applicantToUpdate;
                })
                .map(applicantsRepository::save);
    }

    public void deleteApplicant(int id){
        final var applicantToDelete = applicantsRepository
                 .findById(id)
                 .orElseThrow(() -> generateNotFoundException(id));
        applicantsRepository.deleteById(id);
    }
    private EntityNotFoundException generateNotFoundException(int id) {
        return new EntityNotFoundException("applicant not found with id " + id);
    }
}
