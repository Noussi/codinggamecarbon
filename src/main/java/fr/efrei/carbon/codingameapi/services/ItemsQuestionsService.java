package fr.efrei.carbon.codingameapi.services;

import fr.efrei.carbon.codingameapi.repositories.ItemsQuestionRepository;
import fr.efrei.carbon.codingameapi.repositories.QuestionsRepository;
import fr.efrei.carbon.codingameapi.exceptions.EntityNotFoundException;
import fr.efrei.carbon.codingameapi.models.ItemQuestion;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ItemsQuestionsService {

    public ItemsQuestionRepository itemsQuestionrepository;
    public QuestionsRepository questionsRepository;

    public ItemsQuestionsService(ItemsQuestionRepository itemsQuestionrepository, QuestionsRepository questionsRepository){
        this.itemsQuestionrepository = itemsQuestionrepository;
        this.questionsRepository = questionsRepository;
    }

    public List<ItemQuestion> getItemQuestion(){
        return itemsQuestionrepository.findAll();
    }
    public Optional<ItemQuestion> findItemQuestionById(int questionId) {
        return itemsQuestionrepository.findById(questionId);
    }

    public ItemQuestion createItemQuestion(ItemQuestion itemQuestion) {
        final var question = questionsRepository .findById(itemQuestion.getQuestions().getId())
                .orElseThrow(() -> new EntityNotFoundException("question not found with id " +itemQuestion.getQuestions().getId()));
        final var ItemQuestionToCreate = new ItemQuestion(itemQuestion.getItem1(),itemQuestion.getItem2(),itemQuestion.getItem3(),itemQuestion.getItem4(),question);
        return itemsQuestionrepository.save(ItemQuestionToCreate);
    }

    public void deleteItemQuestion(int id){
        final var ItemQuestionToDelete = itemsQuestionrepository
                .findById(id)
                .orElseThrow(()-> generateNotFoundException(id));
        itemsQuestionrepository.deleteById(id);
    }
    public Optional<ItemQuestion> updateItemQuestion(int id, ItemQuestion itemQuestion){
        return itemsQuestionrepository
                .findById(id)
                .map(itemQuestionToUpdate -> {
                    itemQuestionToUpdate.setItem1(itemQuestion.getItem1());
                    itemQuestionToUpdate.setItem2(itemQuestion.getItem2());
                    itemQuestionToUpdate.setItem3(itemQuestion.getItem3());
                    itemQuestionToUpdate.setItem4(itemQuestion.getItem4());
                    return itemQuestionToUpdate;
                })
                .map(itemsQuestionrepository::save);
    }
    private EntityNotFoundException generateNotFoundException(int id) {
        return new EntityNotFoundException("itemsQuestion not found with id " + id);
    }
}
