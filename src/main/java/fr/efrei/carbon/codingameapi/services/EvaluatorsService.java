package fr.efrei.carbon.codingameapi.services;

import fr.efrei.carbon.codingameapi.repositories.EvaluatorsRepository;
import fr.efrei.carbon.codingameapi.exceptions.EntityNotFoundException;
import fr.efrei.carbon.codingameapi.models.Evaluator;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EvaluatorsService {

    private final EvaluatorsRepository evaluatorsRepository;

    public EvaluatorsService(EvaluatorsRepository evaluatorsRepository) {
        this.evaluatorsRepository = evaluatorsRepository;
    }

    public List<Evaluator> getEvaluators(){
        return  evaluatorsRepository.findAll();
    }
    public Evaluator createEvalutor(Evaluator evaluator) {
        return evaluatorsRepository.save(evaluator);
    }

    public Optional<Evaluator> findById(int id) {
        return evaluatorsRepository.findById(id);
    }

    public Optional<Evaluator> updateEvaluator(int id,Evaluator evaluator) {
        return evaluatorsRepository
                .findById(id)
                .map(evaluatorToUpdate -> {
                    evaluatorToUpdate.setFirstName(evaluator.getFirstName());
                    evaluatorToUpdate.setLastName(evaluator.getLastName());
                    evaluatorToUpdate.setEmail(evaluator.getEmail());
                    evaluatorToUpdate.setFunction(evaluator.getFunction());
                    evaluatorToUpdate.setCompanyName(evaluator.getCompanyName());
                    return evaluatorToUpdate;
                })
                .map(evaluatorsRepository::save);
    }

    public void deleteEvaluator(int id){
        final var evaluatorToDelete = evaluatorsRepository
                .findById(id)
                .orElseThrow(() -> generateNotFoundException(id));
        evaluatorsRepository.deleteById(id);
    }
    private EntityNotFoundException generateNotFoundException(int id) {
        return new EntityNotFoundException("evaluator not found with id " + id);
    }
}
