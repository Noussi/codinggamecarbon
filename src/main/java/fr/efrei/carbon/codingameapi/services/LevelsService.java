package fr.efrei.carbon.codingameapi.services;

import fr.efrei.carbon.codingameapi.repositories.LevelsRepository;
import fr.efrei.carbon.codingameapi.exceptions.EntityNotFoundException;
import fr.efrei.carbon.codingameapi.models.Level;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LevelsService {
    final LevelsRepository levelsRepository;

    public LevelsService(LevelsRepository levelsRepository) {
        this.levelsRepository = levelsRepository;
    }

    public List<Level> getLevels() {
        return levelsRepository.findAll();
    }

    public Level createLevel(Level level) {
        return levelsRepository.save(level);
    }

    public Optional<Level> findById(int id) {
        return levelsRepository.findById(id);
    }

    public Optional<Level> updateLevel(int id, Level level) {
        return levelsRepository
                .findById(id)
                .map(levelToUpdate -> {
                    levelToUpdate.setName(level.getName());
                    return levelToUpdate;
                })
                .map(levelsRepository::save);
    }

    public void deleteLevel(int id) {
        final var levelToDelete = levelsRepository
                .findById(id)
                .orElseThrow(() -> generateNotFoundException(id));

        levelsRepository.delete(levelToDelete);
    }

    private EntityNotFoundException generateNotFoundException(int id) {
        return new EntityNotFoundException("level not found with id " + id);
    }
}
