package fr.efrei.carbon.codingameapi.services;

import fr.efrei.carbon.codingameapi.repositories.QuestionsRepository;
import fr.efrei.carbon.codingameapi.repositories.TechnologiesRepository;
import fr.efrei.carbon.codingameapi.exceptions.EntityNotFoundException;
import fr.efrei.carbon.codingameapi.models.Question;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class QuestionsService {

    private final QuestionsRepository questionRepository;
    private final TechnologiesRepository technologiesRepository;

    public QuestionsService(QuestionsRepository questionRepository, TechnologiesRepository technologiesRepository) {
        this.questionRepository = questionRepository;
        this.technologiesRepository = technologiesRepository;
    }

    public List<Question> getQuestions() {
        return questionRepository.findAll();
    }

    public Optional<Question> findQuestionById(int questionId) {
        return questionRepository.findById(questionId);
    }

    public Question createQuestion(Question question) {
        final var technology = technologiesRepository.findById(question.getTechnology().getId())
                .orElseThrow(() -> new EntityNotFoundException("technology not found with id " + question.getTechnology().getId()));
        final var questionToCreate = new Question(question.getResponse(), question.getWording(), question.getLevel(), technology, List.of());
        return questionRepository.save(questionToCreate);
    }

    public void deleteQuestion(int id) {
        final var QuestionToDelete = questionRepository
                .findById(id)
                .orElseThrow(() -> generateNotFoundException(id));
        questionRepository.deleteById(id);
    }

    public Optional<Question> updateQuestion(int id, Question question) {
        return questionRepository
                .findById(id)
                .map(questionToUpdate -> {
                    return new Question(question.getResponse(), question.getWording(), question.getLevel(), question.getTechnology(), question.getQuestions());
                })
                .map(questionRepository::save);
    }

    private EntityNotFoundException generateNotFoundException(int id) {
        return new EntityNotFoundException("Technology not found with id " + id);
    }
}
