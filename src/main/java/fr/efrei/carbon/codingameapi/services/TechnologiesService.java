package fr.efrei.carbon.codingameapi.services;

import fr.efrei.carbon.codingameapi.exceptions.EntityNotFoundException;
import fr.efrei.carbon.codingameapi.repositories.TechnologiesRepository;
import fr.efrei.carbon.codingameapi.models.Technology;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TechnologiesService {

    private final TechnologiesRepository technologiesRepository;

    public TechnologiesService(TechnologiesRepository technologiesRepository) {
        this.technologiesRepository = technologiesRepository;
    }

    public List<Technology> getTechnologies() {
        return technologiesRepository.findAll();
    }

    public Technology createTechnology(Technology technology) {
        return technologiesRepository.save(technology);
    }

    public Optional<Technology> findById(int id) {
        return technologiesRepository.findById(id);
    }

    public Optional<Technology> updateTechnology(int id, Technology technology) {
        return technologiesRepository
                .findById(id)
                .map(technologyToUpdate -> {
                    technologyToUpdate.setName(technology.getName());
                    return technologyToUpdate;
                })
                .map(technologiesRepository::save);
    }

    public void deleteTechnology(int id) {
        final var technologyToDelete = technologiesRepository
                .findById(id)
                .orElseThrow(() -> generateNotFoundException(id));

        technologiesRepository.delete(technologyToDelete);
    }

    private EntityNotFoundException generateNotFoundException(int id) {
        return new EntityNotFoundException("technology not found with id " + id);
    }
}
