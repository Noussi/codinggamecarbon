package fr.efrei.carbon.codingameapi.creationRequest;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CampaignCreationRequest {

    private final String name;
    private final int levelId;
    //private final List<Integer> technologyIdList;

    @JsonCreator
    public CampaignCreationRequest(@JsonProperty("name") String name,
                                   @JsonProperty("level") int levelId) {
        this.name = name;
        this.levelId = levelId;
    }

    public String getName() {
        return name;
    }

    public int getLevelId() {
        return levelId;
    }

   /* public List<Integer> getTechnologyId() {
        return technologyIdList;
    }*/
}
