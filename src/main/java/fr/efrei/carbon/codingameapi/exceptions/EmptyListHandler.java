package fr.efrei.carbon.codingameapi.exceptions;

public class EmptyListHandler extends Exception{

    public EmptyListHandler(String message, boolean b) throws RuntimeException {
        super(message);
        try {
            this.EmptyListException(b, message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //public EmptyReturnListHandler
    public void EmptyListException(Boolean b, String message) throws Exception {
        if(b){
            throw new Exception(message);
        }
    }
}
