package fr.efrei.carbon.codingameapi;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.init.Jackson2RepositoryPopulatorFactoryBean;

@SpringBootApplication
public class CodingGameApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(CodingGameApiApplication.class, args);
    }

    @Bean
    Jackson2RepositoryPopulatorFactoryBean jackson2RepositoryPopulatorFactoryBean(
            @Value("classpath:data-seeds/languages.json") Resource languagesFile,
            @Value("classpath:data-seeds/levels.json") Resource levelsFile,
            @Value("classpath:data-seeds/technologies.json") Resource technologiesFile
    ) {
        final var factoryBean = new Jackson2RepositoryPopulatorFactoryBean();
        factoryBean.setResources(
                new Resource[]{
                        languagesFile,
                        levelsFile,
                        technologiesFile
                }
        );
        return factoryBean;
    }
}
