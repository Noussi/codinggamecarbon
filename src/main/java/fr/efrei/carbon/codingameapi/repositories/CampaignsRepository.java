package fr.efrei.carbon.codingameapi.repositories;

import fr.efrei.carbon.codingameapi.models.Campaign;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CampaignsRepository extends JpaRepository<Campaign, Integer> {
}

