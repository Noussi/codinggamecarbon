package fr.efrei.carbon.codingameapi.repositories;

import fr.efrei.carbon.codingameapi.models.Applicant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicantsRepository extends JpaRepository<Applicant, Integer> {
}
