package fr.efrei.carbon.codingameapi.repositories;

import fr.efrei.carbon.codingameapi.models.Technology;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TechnologiesRepository extends JpaRepository<Technology, Integer> {
}
