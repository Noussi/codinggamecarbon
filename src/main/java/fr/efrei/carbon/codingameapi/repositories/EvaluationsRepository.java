package fr.efrei.carbon.codingameapi.repositories;

import fr.efrei.carbon.codingameapi.models.Evaluation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EvaluationsRepository extends JpaRepository<Evaluation, Integer> {
}
