package fr.efrei.carbon.codingameapi.repositories;

import fr.efrei.carbon.codingameapi.models.Question;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionsRepository extends JpaRepository<Question, Integer> {
}
