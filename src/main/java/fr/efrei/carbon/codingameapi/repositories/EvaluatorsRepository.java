package fr.efrei.carbon.codingameapi.repositories;

import fr.efrei.carbon.codingameapi.models.Evaluator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EvaluatorsRepository extends JpaRepository<Evaluator, Integer> {
}
