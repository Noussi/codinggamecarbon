package fr.efrei.carbon.codingameapi.repositories;

import fr.efrei.carbon.codingameapi.models.ItemQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemsQuestionRepository extends JpaRepository<ItemQuestion, Integer> {

}
