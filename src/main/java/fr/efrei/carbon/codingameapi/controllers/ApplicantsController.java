package fr.efrei.carbon.codingameapi.controllers;

import fr.efrei.carbon.codingameapi.services.ApplicantsService;
import fr.efrei.carbon.codingameapi.models.Applicant;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("applicants")
public class ApplicantsController {

    private final ApplicantsService applicantService;

    public ApplicantsController(ApplicantsService applicantService) {
        this.applicantService = applicantService;
    }

    @GetMapping
    public List<Applicant> getApplicants() {
        return this.applicantService.getApplicants();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Applicant> findApplicant(@PathVariable int id) {
        return ResponseEntity.of(applicantService.findById(id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Applicant> updateApplicant(@PathVariable int id,
                                                     @RequestBody Applicant applicant) {
        return ResponseEntity.of(this.applicantService.updateApplicant(id, applicant));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Applicant createApplicant(@RequestBody Applicant applicant) {
        return applicantService.createApplicant(applicant);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteApplicant(@PathVariable int id){
        applicantService.deleteApplicant(id);
    }
}
