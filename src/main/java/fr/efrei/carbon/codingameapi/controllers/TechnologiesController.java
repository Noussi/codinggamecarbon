package fr.efrei.carbon.codingameapi.controllers;

import fr.efrei.carbon.codingameapi.models.Technology;
import fr.efrei.carbon.codingameapi.services.TechnologiesService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("technology")
@RestController
public class TechnologiesController {
    private final TechnologiesService technologiesService;

    public TechnologiesController(TechnologiesService technologiesService) {
        this.technologiesService = technologiesService;
    }

    @GetMapping
    public List<Technology> getTechnologies(){
        return technologiesService.getTechnologies();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Technology> updateTechnology(@PathVariable int id, @RequestBody Technology technology) {
        return ResponseEntity.of(technologiesService.updateTechnology(id,technology));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Technology createTechnology(@RequestBody Technology technology) {
        return technologiesService.createTechnology(technology);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Technology> findTechnology(@PathVariable int id) {
        return ResponseEntity.of(technologiesService.findById(id));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTechnology(@PathVariable int id) {
        technologiesService.deleteTechnology(id);
    }
}
