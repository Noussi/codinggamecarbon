package fr.efrei.carbon.codingameapi.controllers;

import fr.efrei.carbon.codingameapi.models.Applicant;
import fr.efrei.carbon.codingameapi.models.Evaluator;
import fr.efrei.carbon.codingameapi.services.EvaluatorsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("evaluators")
@RestController
public class EvaluatorsController {

    private final EvaluatorsService evaluatorsService;

    public EvaluatorsController(EvaluatorsService evaluatorsService) {
        this.evaluatorsService = evaluatorsService;
    }

    @GetMapping
    public List<Evaluator> getEvaluators(){
        return evaluatorsService.getEvaluators();
    }
    @GetMapping("/{id}")
    public ResponseEntity<Evaluator> findEvaluator(@PathVariable int id) {
        return ResponseEntity.of(evaluatorsService.findById(id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Evaluator> updateEvaluator(@PathVariable int id,
                                                     @RequestBody Evaluator evaluator) {
        return ResponseEntity.of(this.evaluatorsService.updateEvaluator(id, evaluator));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Evaluator createEvaluator(@RequestBody Evaluator evaluator) {
        return evaluatorsService.createEvalutor(evaluator);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEvaluator(@PathVariable int id){
         evaluatorsService.deleteEvaluator(id);
    }
}

