package fr.efrei.carbon.codingameapi.controllers;
import fr.efrei.carbon.codingameapi.models.ItemQuestion;
import fr.efrei.carbon.codingameapi.services.ItemsQuestionsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RequestMapping("itemQuestions")
@RestController
public class ItemsQuestionsController {
    private ItemsQuestionsService itemsQuestionService;

    public ItemsQuestionsController(ItemsQuestionsService itemsQuestionService){
        this.itemsQuestionService = itemsQuestionService;
    }
    @GetMapping
    public List<ItemQuestion> getItemQuestions(){
        return itemsQuestionService.getItemQuestion();
    }
    @GetMapping("/{id}")
    public ResponseEntity<ItemQuestion> findItemQuestion(@PathVariable int id) {
        return ResponseEntity.of(itemsQuestionService.findItemQuestionById(id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<ItemQuestion> updateItemQuestion(@PathVariable int id,
                                                   @RequestBody ItemQuestion itemQuestion) {
        return ResponseEntity.of(this.itemsQuestionService.updateItemQuestion(id,itemQuestion));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ItemQuestion createItemQuestion(@RequestBody ItemQuestion itemQuestion) {
        return itemsQuestionService.createItemQuestion(itemQuestion);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteItemQuestion(@PathVariable int id){
        itemsQuestionService.deleteItemQuestion(id);
    }
}
