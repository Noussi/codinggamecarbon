package fr.efrei.carbon.codingameapi.controllers;

import fr.efrei.carbon.codingameapi.models.Evaluation;
import fr.efrei.carbon.codingameapi.services.EvaluationsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("evaluations")
public class EvaluationsController {
     private final EvaluationsService evaluationsService;

    public EvaluationsController(EvaluationsService evaluationsService) {
        this.evaluationsService = evaluationsService;
    }

    @GetMapping
    public List<Evaluation> getEvaluations(){
        return evaluationsService.getEvaluations();
    }
}
