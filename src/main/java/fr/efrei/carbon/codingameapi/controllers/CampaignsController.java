package fr.efrei.carbon.codingameapi.controllers;

import fr.efrei.carbon.codingameapi.creationRequest.CampaignCreationRequest;
import fr.efrei.carbon.codingameapi.services.CampaignService;
import fr.efrei.carbon.codingameapi.models.Campaign;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("campaigns")
public class CampaignsController {

    private final CampaignService campaignService;

    public CampaignsController(CampaignService campaignService) {
        this.campaignService = campaignService;
    }

    @GetMapping
    public List<Campaign> getAllCampaigns() {
        System.out.println("hello1");
        return campaignService.getAllCampaigns();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Campaign> getCampaignsById(@PathVariable("id") int campaignId) {
        return ResponseEntity.of(campaignService.findCampaignById(campaignId));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Campaign> updateCampaign(@PathVariable int id,
                                                   @RequestBody Campaign campaign){
        return ResponseEntity.of(this.campaignService.updateCampaign(id,campaign));
    }
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCampaign(@PathVariable int id){
        campaignService.deleteCampaign(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Campaign create(@RequestBody Campaign campaign) {
        return campaignService.createCampaign(campaign);
    }
}
