package fr.efrei.carbon.codingameapi.controllers;

import fr.efrei.carbon.codingameapi.models.Question;
import fr.efrei.carbon.codingameapi.services.QuestionsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("questions")
public class QuestionsController {

    private final QuestionsService questionsService;

    public QuestionsController(QuestionsService questionsService) {
        this.questionsService = questionsService;
    }

    @GetMapping
    public List<Question> getQuestions(){
        return this.questionsService.getQuestions();
    }
    @GetMapping("/{id}")
    public ResponseEntity<Question> findQuestion(@PathVariable int id) {
        return ResponseEntity.of(questionsService.findQuestionById(id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Question> updateQuestion(@PathVariable int id,
                                                     @RequestBody Question Question) {
        return ResponseEntity.of(this.questionsService.updateQuestion(id, Question));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Question createQuestion(@RequestBody Question Question) {
        return questionsService.createQuestion(Question);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteQuestion(@PathVariable int id){
        questionsService.deleteQuestion(id);
    }
}
