package fr.efrei.carbon.codingameapi.controllers;

import fr.efrei.carbon.codingameapi.services.LevelsService;
import fr.efrei.carbon.codingameapi.models.Level;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("levels")
@RestController
public class LevelsController {
    private final LevelsService levelsService;

    public LevelsController(LevelsService levelsService) {
        this.levelsService = levelsService;
    }

    @GetMapping
    public List<Level> getLevels() {
        return levelsService.getLevels();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Level> updateLevel(@PathVariable int id, @RequestBody Level level) {
        return ResponseEntity.of(this.levelsService.updateLevel(id, level));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Level createLevel(@RequestBody Level level) {
        return levelsService.createLevel(level);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Level> findLevel(@PathVariable int id) {
        return ResponseEntity.of(levelsService.findById(id));
    }

    @DeleteMapping("/{id}")
    public void deleteLevel(@PathVariable int id) {
        levelsService.deleteLevel(id);
    }
}
